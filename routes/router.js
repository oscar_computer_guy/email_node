const express = require("express");

const router = express.Router();

const sendEmail = require("../sendEmail").sendEmail;

router.get("/", function(req, res) {
  res.json({ message: "Welcome to the email api" });
});

router.post("/email", sendEmail);

module.exports = router;
